package com.example.android.quakereport;


public class Earthquake {
    private double mMagnitude;

    private String mLocation;

    private String mDate;

    private String mTime;

    private String mUrl;

    public Earthquake(double magnitude, String location, String date, String time, String url){
        mMagnitude = magnitude;
        mLocation = location;
        mDate = date;
        mTime = time;
        mUrl = url;
    }

    public String getDate() {
        return mDate;
    }

    public String getLocation() {
        return mLocation;
    }

    public double getMagnitude() {
        return mMagnitude;
    }

    public String getTime() {
        return mTime;
    }

    public String getUrl(){
        return mUrl;
    }
}
